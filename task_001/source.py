ss = input().split()
temp = int(ss[0])
cool = int(ss[1])
heat = int(ss[2])
n = int(input())
ans = []
for k in range(n):
    s = input()
    a = ""
    yk = 0
    cur_temp = ""
    price = ""
    for j in range(len(s) - 1, -1,-1):
        if s[j].isalpha():
            name = s[:j + 1]
            break
        elif s[j] == ' ':
            if (yk == 0 and len(a) > 0):
                cur_temp = int(a)
                yk += 1
            elif (yk == 1 and len(a) > 0):
                price = int(a)
            a = ''
        else:
            a = s[j] + a
    if (cur_temp > temp):
        ans.append([(cur_temp - temp) * cool * price, name])
    else:
        ans.append([(temp - cur_temp) * heat * price, name])
ans.sort()
for i in ans:
    print(i[1],i[0])