#include<bits/stdc++.h>

using namespace std;
typedef long long ll;

ll M1, M2, M;

vector<ll> S1, S2;
vector<string> v;
vector<vector<ll> > H;
int n, m, h, w;

inline ll binprod (ll a, ll n) {
    ll res = 0LL;
    while (n) {
            res = (res + (n%2) * a) % M;
        a = (2LL * a) % M;
        n /= 2;
    }
    return res;
}

inline ll hsh(int x1, int y1, int x2, int y2){
    ll r = H[x2][y2];
    if(x1 > 0) r = (r + (M -  H[x1-1][y2])) % M;
    if(y1 > 0) r = (r + (M -  H[x2][y1-1])) % M;
    if(x1 > 0 && y1 > 0) r = (r + H[x1-1][y1-1]) % M;

    return r;
}

inline ll hrt(int x1, int y1, int h, int w){
    int x2 = x1 + h - 1;
    int y2 = y1 + w -1;
    ll ty =  hsh(x1, y1, x2, y2);
    ty = binprod(ty, S1[n - h - x1]);
    ty = binprod(ty, S2[m - w - y1]);

    return ty;
}

int main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    cin >> M1 >> M2 >> M;
    cin >> n >> m;
    v.resize(n);
    for(int i = 0; i < n; i++) cin >> v[i];
    cin >> h >> w;

    ll st = 1;
    for(int i = 0; i < 510; i++){
        S1.push_back(st);
        st = binprod(st, M1);
    }

    st = 1;
    for(int i = 0; i < 510; i++){
        S2.push_back(st);
        st = binprod(st, M2);
    }

    H.resize(n, vector<ll>(m));
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++){
            H[i][j] = binprod(binprod((v[i][j] - '0'), S1[i]),  S2[j]);
            if(i > 0) H[i][j] = (H[i][j] + H[i-1][j]) % M;
            if(j > 0) H[i][j] = (H[i][j] + H[i][j-1]) % M;
            if(i > 0 && j > 0) H[i][j] = (H[i][j] + (M - H[i-1][j-1])) % M;
    }

    for(int i = 0; i < n - h + 1; i++){
        for(int j = 0; j < m - w + 1; j++)
        cout<<hrt(i, j, h, w)<<' ';
        cout<<endl;
    }
}