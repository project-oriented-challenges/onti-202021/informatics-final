n = int(input())
control = 10**20
ans = 1
for k in range(1, n + 1):
    nn = n // k
    f = n - n // k * k
    if (control > nn * (nn + 1) // 2 * k * (k + 1) // 2 + f * (f + 1) // 2 + f * (f + 1) // 2 * nn):
        control = nn * (nn + 1) // 2 * k * (k + 1) // 2 + f * (f + 1) // 2 +  nn  * f * (f + 1) // 2
        ans = k
print(ans)