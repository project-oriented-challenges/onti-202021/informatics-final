#include<bits/stdc++.h>

using namespace std;
typedef long long ll;

ll P1 = 1073676287;
ll P = 1125899839733759;
vector<ll> H, S1, RH;
string s;
int n;

inline ll binprod (ll a, ll n) {
    ll res = 0LL;
    while (n) {
            res = (res + (n%1024) * a) % P;
        a = (1024LL * a) % P;
        n /= 1024;
    }
    return res;
}

inline ll hsh(int x1, int x2, vector<ll> &H){
    ll r = H[x2];
    if(x1 > 0) r = (r + (P -  H[x1-1])) % P;

    return r;
}

inline ll hsub(int x1, int w, vector<ll> &H){
    int x2 = x1 + w - 1;
    ll ty =  hsh(x1, x2, H);
    ty = binprod(ty, S1[n - w - x1]);

    return ty;
}

int main(){
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);

    cin >> s;
    n = s.size();
    string rs = s;
    reverse(rs.begin(), rs.end());

    ll st = 1;
    for(int i = 0; i < n; i++){
        S1.push_back(st);
        st = binprod(st, P1);
    }

    H.resize(n);
    RH.resize(n);
    for(int i = 0; i < n; i++){
        H[i] = binprod((s[i] - 'a' + 1), S1[i]);
        if(i > 0) H[i] = (H[i] + H[i-1]) % P;

        RH[i] = binprod((rs[i] - 'a' + 1), S1[i]);
        if(i > 0) RH[i] = (RH[i] + RH[i-1]) % P;
    }

    int mxL = 1, rD = 0;
    int L = 1, R = n + 1;
    while(R - L > 1){
        ll M = (R + L) / 2;

        set<ll> mask;
        bool p = 0;
        for(int i = 0; i < n - M + 1; i++){
            mask.insert(hsub(i, M, RH));
        }

        for(int i = 0; i < n - M + 1; i++){
            ll y = hsub(i, M, H);

            if(mask.find(y) != mask.end()){
                p = 1;
                if(M > mxL){
                    mxL = M;
                    rD = i;
                }
                break;
            }
        }
        if(p) L = M; else R = M;
    }

    cout<<s.substr(rD, L);
}