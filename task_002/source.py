n = int(input())
s = input()
dp = [0] * 100
dp[1] = 1
for i in range(2, 60):
    dp[i] = dp[i - 1] * 2 + 1
ans1 = 0
ans2 = 0
for i in range(n):
    if (s[i] == 'A'):
        ans1 += dp[i] + 1
    else:
        ans2 += dp[i] + 1
print(min(ans1, ans2), max(ans1,ans2))